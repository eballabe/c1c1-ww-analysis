#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
import os, string, pickle, copy
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

variable = 'METsig'
region = 'CR_Dib_DF0J_DM100_90'
outputDir = '/Users/eric/Desktop/MultiBinPlots'

# dictionary for plotting cosmetics --
Plots = {  'MET'    : {'PlotNBins' : 30, 'PlotBinLow' : 0, 'PlotBinHigh' : 300, 'titleX' : 'E_{T}^{miss} [GeV]'},
           'METsig' : {'PlotNBins' : 20, 'PlotBinLow' : 0, 'PlotBinHigh' : 20, 'titleX' : 'E_{T}^{miss} significance'},
           'lep1pT' : {'PlotNBins' : 25, 'PlotBinLow' : 0., 'PlotBinHigh' : 100., 'titleX' : 'Leading lepton p_{T} [GeV]'},
           'lep2pT' : {'PlotNBins' : 25, 'PlotBinLow' : 0., 'PlotBinHigh' : 100., 'titleX' : 'Subleading lepton p_{T} [GeV]'},
           'njet'   : {'PlotNBins' : 3, 'PlotBinLow' : 0., 'PlotBinHigh' : 3., 'titleX' : 'Number of jets'},
           'nbjet'  : {'PlotNBins' : 3, 'PlotBinLow' : 0., 'PlotBinHigh' : 3., 'titleX' : 'Number of b-jets'},
        }

def main():

    # Where's the workspace file? 
    wsfilename = "/Users/eric/results/BkgOnly_noSyst_0/BkgOnly_combined_BasicMeasurement_model_afterFit.root" # 

    # Where's the pickle file?
    pickleFilename = "/Users/eric/MyYieldsTable_{}.pickle".format(variable)
    
    # Run blinded?
    doBlind = True

    # Used as plot title
    region = ""

    # Samples to stack on top of eachother in each region
    samples = "MCFakes,VV,ttbar,Wt,Zjets,Zttjets,VVV,other"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    if not os.path.exists(pickleFilename):
        print "pickle filename %s does not exist" % pickleFilename
        print "will proceed to run yieldstable again"
        
        # Run YieldsTable.py with all regions and samples requested, -P command is MANDATORY for multibin plot
        cmd = "YieldsTable.py -c %s -s %s -w %s -P -o MyYieldsTable_%s.tex" % (",".join(regionList), samples, wsfilename, variable)
        print cmd
        subprocess.call(cmd, shell=True)

    if not os.path.exists(pickleFilename):
        print "pickle filename %s still does not exist" % pickleFilename
        return
    
    regionList = []
    NBins = Plots[variable]['PlotNBins']
    for bin in range(NBins):
        regionList.append("{}_{}_bin{}".format(region,variable,bin))


    # Open the pickle and make the pull plot
    MymakePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)


# Build a dictionary that remaps region names
def renameRegions():

    myRegionDict = {}


    NBins = Plots[variable]['PlotNBins']
    for bin in range(NBins):
        myRegionDict["{}_{}_bin{}".format(region,variable,bin)] = "{}".format(bin)

    # Remap region names using the old name as index, e.g.:
    myRegionDict["CR_Dib_DF0J_DM100_90"] = "CR_VV_DF0J"
    myRegionDict["CR_Dib_DF1J_DM100_90"] = "CR_VV_DF1J"
    myRegionDict["CR_Dib_SF0J_DM100_90"] = "CR_VV_SF0J"
    myRegionDict["CR_Dib_SF1J_DM100_90"] = "CR_VV_SF1J"
    myRegionDict["CR_Top_DF_DM100_90"] = "CR_Top_DF"
    myRegionDict["CR_Top_SF_DM100_90"] = "CR_Top_SF"
    myRegionDict["VR_Dib_DF0J_DM100_90"] = "VR_VV_DF0J"
    myRegionDict["VR_Dib_DF1J_DM100_90"] = "VR_VV_DF1J"
    myRegionDict["VR_Dib_SF0J_DM100_90"] = "VR_VV_SF0J"
    myRegionDict["VR_Dib_SF1J_DM100_90"] = "VR_VV_SF1J"
    myRegionDict["VR_Top_DF_DM100_90"] = "VR_Top_DF"
    myRegionDict["VR_Top_SF_DM100_90"] = "VR_Top_SF"
    
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]
    
    regionList = ["{}_{}".format(region,variable)] # here bins are not specified

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SLWR") != -1: return kBlue+3
    if name.find("SLTR") != -1: return kBlue+3
    if name.find("SR") != -1: return kOrange
    if name.find("SLVR")  != -1: return kOrange
    if name.find("SSloose")  != -1: return kOrange
    if name.find("SS_") != -1: return kRed
 
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "MCFakes":         return TColor.GetColor('#fde0dd')
    if sample == "VV":         return TColor.GetColor('#fe9929')
    if sample == "ttbar":         return TColor.GetColor('#08306b')
    if sample == "Wt":         return TColor.GetColor('#0868ac')
    if sample == "Zjets":         return TColor.GetColor('#41ab5d')
    if sample == "Zttjets":         return TColor.GetColor('#c7e9c0')
    if sample == "VVV":         return TColor.GetColor('#fec49f')
    if sample == "other":         return TColor.GetColor('#9ecae1')
    else:
        print "cannot find color for sample (",sample,")"

    return 1


# Methods from PullPlotUtils
def MyMakeHistPullPlot(samples, regionList, outFileNamePrefix, hresults, renamedRegions, doBlind, outDir=outputDir, minimum=0.1, maximum=None, logy=True, doSignificance=False):
    print "========================================", outFileNamePrefix
    ROOT.gStyle.SetOptStat(0000);
    Npar=len(regionList)

    hdata = TH1F(outFileNamePrefix, outFileNamePrefix, Npar, 0, Npar);
    hdata.GetYaxis().SetTitle("Events")
    hdata.GetYaxis().SetTitleSize( 0.065 )
    hdata.GetYaxis().SetTitleOffset( 0.8 )
    hdata.GetXaxis().SetLabelSize( 0.06 )
    hdata.GetYaxis().SetLabelSize( 0.05 )
    hdata.SetMarkerStyle(20)
    hbkg = TH1F("hbkg", "hbkg", Npar, 0, Npar);
    hbkg.SetLineColor(1)
    hbkg.SetLineWidth(2)

    hbkgUp = TH1F("hbkgUp", "hbkgUp", Npar, 0, Npar);
    hbkgUp.SetLineStyle(0)
    hbkgUp.SetLineWidth(0)

    hbkgDown = TH1F("hbkgDown", "hbkgDown", Npar, 0, Npar);
    hbkgDown.SetLineStyle(0)
    hbkgDown.SetLineWidth(0)

    hbkgComponents = []
    samples.replace(" ","") #remove spaces, and split by comma => don't split by ", "
    for sam in samples.split(","):
        h = TH1F("hbkg"+sam, "hbkg"+sam, Npar, 0, Npar)
        h.SetFillColor(getSampleColor(sam))
        hbkgComponents.append(h)

    graph_bkg = TGraphAsymmErrors(Npar)
    graph_bkg.SetFillColor(1)
    graph_bkg2 = TGraphAsymmErrors(Npar)
    graph_bkg2.SetFillColor(1)
    graph_bkg2.SetLineWidth(2)
    graph_bkg2.SetFillStyle(3004)

    graph_bkg3 = TGraphAsymmErrors(Npar)
    graph_bkg3.SetFillColor(kCyan+1)
    graph_data = TGraphAsymmErrors(Npar)
    graph_data.SetFillColor(kCyan+1)

    graph_pull = TGraphAsymmErrors(Npar)

    MakeHist(regionList,  renamedRegions,  hresults, hdata, hbkg, hbkgDown, hbkgUp, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents)

    myleg = TLegend(0.49,0.60,0.81,0.84)
    myleg.SetNColumns(2)
    myleg.SetBorderSize(0)
    myleg.SetFillStyle(0)
    myleg.AddEntry(graph_data, renamedRegions.get("data", "data"), "p")
    myleg.AddEntry(graph_bkg2, renamedRegions.get("bkg2", "total background"), "fl")
    # add background in same order as above
    for h, sample in zip(hbkgComponents, samples.split(",")):
      myleg.AddEntry(h, renamedRegions.get(sample, sample), "f")

    c = TCanvas("c"+outFileNamePrefix, outFileNamePrefix, 600, 600);
    upperPad = ROOT.TPad("upperPad", "upperPad", 0.001, 0.30, 0.995, 0.995)
    lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.001, 0.001, 0.995, 0.30)

    upperPad.SetFillColor(0);
    upperPad.SetBorderMode(0);
    upperPad.SetBorderSize(2);
    upperPad.SetTicks()
    upperPad.SetTopMargin   ( 0.1 );
    upperPad.SetRightMargin ( 0.11 );
    upperPad.SetBottomMargin( 0.0025 );
    upperPad.SetLeftMargin( 0.12 );
    upperPad.SetFrameBorderMode(0);
    upperPad.SetFrameBorderMode(0);
    if logy: upperPad.SetLogy()
    upperPad.Draw()

    #lowerPad.SetGridx();
    #lowerPad.SetGridy();
    #lowerPad.SetFillColor(0);
    lowerPad.SetBorderMode(0);
    lowerPad.SetBorderSize(2);
    #lowerPad.SetTickx(25);
    #lowerPad.SetTicky(25);
    lowerPad.SetTopMargin   ( 0.003 );
    lowerPad.SetRightMargin ( 0.11 );
    lowerPad.SetBottomMargin( 0.5 );
    lowerPad.SetLeftMargin( 0.12 );
    lowerPad.Draw()

    c.SetFrameFillColor(ROOT.kWhite)

    upperPad.cd()

    if minimum: hdata.SetMinimum(minimum)
    if maximum: hdata.SetMaximum(maximum)
    hdata.Draw("E0")
    myleg.Draw()
    stack = THStack("stack","stack")
    for h in reversed(hbkgComponents):
        stack.Add(h)
    stack.Draw("same")
    hbkg.Draw("hist,same")
    hbkgUp.Draw("hist,same")
    hbkgDown.Draw("hist,same")

    graph_bkg2.Draw("2")
    graph_data.SetMarkerStyle(20)
    #graph_data.Draw("E0,P,Z")
    graph_data.Draw("E0,Z")
    hdata.Draw("E0,same")
    hdata.Draw("E0,same,axis")

    lumiText = TLatex()
    lumiText.SetNDC()
    lumiText.SetTextAlign( 11 )
    lumiText.SetTextFont( 42 )
    lumiText.SetTextSize( 0.05 )
    lumiText.SetTextColor( 1 )
    lumiText.DrawLatex(0.15,0.8, "#bf{#it{ATLAS}} Internal")
    lumiText.DrawLatex(0.15, 0.74, "#sqrt{{s}}=13 TeV, {0:0.1f} fb^{{-1}}".format(getattr(MyMakeHistPullPlot,'luminosity',139.0)))
    #lumiText.SetTextSize( 0.05 )   change size if needed
    lumiText.DrawLatex(0.15, 0.68, "{}".format(renamedRegions[region]))

    lowerPad.cd()
    
    # Draw frame with pulls
    if doSignificance:
        #frame = GetFrame(outFileNamePrefix,Npar,"Significance",horizontal=True)
        print('not implemented')
    else:
        hRatio = TH1F("hRatio","",Npar, Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh'])
        hShadow = TH1F("hShadow","",Npar, Plots[variable]['PlotBinLow'],Plots[variable]['PlotBinHigh'])

        for i in range(1, Npar+1):
            xSM = stack.GetStack().Last().GetBinContent(i)
            errxSM = stack.GetStack().Last().GetBinError(i)
            xData = hdata.GetBinContent(i)
            errxData = hdata.GetBinError(i)
            print("i={}, xSM={}, xData={}, errData={}, errSM={}".format(i,xSM,xData,errxData,errxSM))

            if xSM>0.03:
               hRatio.SetBinContent(i,xData/xSM)
               hRatio.SetBinError(i,errxData/xSM)
               hShadow.SetBinContent(i,1)
               hShadow.SetBinError(i,errxSM/xSM)              

        hRatio.SetMarkerStyle(20)

        hShadow.SetLineColor(0)
        hShadow.SetFillColor(1)
        hShadow.SetFillStyle(3004)
        hShadow.SetLineColor(1)
        hShadow.SetMarkerSize(0)
        hShadow.SetMarkerColor(0)

        hShadow.GetXaxis().SetTitle(Plots[variable]['titleX'])
        hShadow.GetXaxis().SetTitleSize( 0.12 )
        hShadow.GetXaxis().SetTitleOffset( 1.3 )

        hShadow.GetYaxis().SetTitle("DATA/MC")
        hShadow.GetYaxis().SetTitleSize( 0.12 )
        hShadow.GetYaxis().SetTitleOffset( 0.4 )
        hShadow.GetXaxis().SetLabelSize( 0.1 )
        hShadow.GetYaxis().SetLabelSize( 0.08 )
        hShadow.Draw("E2")
        hRatio.Draw("SAME")



    #GetBoxes(all, hresults, renamedRegions, frame, doBlind, True, doPreFit=bool("CR" in outFileNamePrefix))
    
    if doBlind:
        c.Print(os.path.join(outDir, "{}_blindSR.eps".format(variable)))
        c.Print(os.path.join(outDir, "{}_blindSR.png".format(variable)))
        c.Print(os.path.join(outDir, "{}_blindSR.pdf".format(variable)))
    else:
        c.Print(os.path.join(outDir, "{}.eps".format(variable)))
        c.Print(os.path.join(outDir, "{}.png".format(variable)))
        c.Print(os.path.join(outDir, "{}.pdf".format(variable)))

    return


def PoissonError(obs):
    """
    Return the Poisson uncertainty on a number

    @param obs The number to calculate the uncertainty for
    reference from ATLAS Statistics Forum: http://www.pp.rhul.ac.uk/~cowan/atlas/ErrorBars.pdf
    """
    posError = TMath.ChisquareQuantile(1. - (1. - 0.68)/2. , 2.* (obs + 1.)) / 2. - obs - 1
    negError = obs - TMath.ChisquareQuantile((1. - 0.68)/2., 2.*obs) / 2
    symError = abs(posError-negError)/2.
    return (posError,negError,symError)


def MakeHist(regionList, renamedRegions, results, hdata, hbkg, hbkgUp, hbkgDown, graph_bkg, graph_bkg2, graph_bkg3, graph_data, graph_pull, hbkgComponents):
    max = 0
    min = 99999999999.
    for counter in range(len(regionList)):#loop over all the regions
        nObs = 0
        nExp = 0
        nExpEr = 0
        nExpTotEr = 0
        nExpStatEr = 0
        nExpStatErUp = 0
        nExpStatErDo = 0
        nExpTotErUp = 0
        nExpTotErDo = 0
        pull = 0

        name = regionList[counter].replace(" ","")
        if name in renamedRegions.keys():
            name = renamedRegions[name]

        for info in results: #extract the information
            if regionList[counter] in info[0]:
                nObs = info[2]
                nExp = info[3]
                nExpEr = info[4]

                if nExp>0:
                    nExpStatEr = sqrt(nExp)
                pEr = PoissonError(nObs)
                nExpStatErUp = pEr[0]
                nExpStatErDo = pEr[1]

                if name.find("CR") < 0:
                    nExpTotEr = sqrt(nExpEr*nExpEr)
                    nExpTotErUp = sqrt(nExpEr*nExpEr)
                    nExpTotErDo = sqrt(nExpEr*nExpEr)
                else:
                    nExpTotEr = nExpEr
        
                if (nObs-nExp) >= 0 and nExpTotErUp != 0:
                    pull = (nObs-nExp)/nExpTotErUp
                if (nObs-nExp) <= 0 and nExpTotErDo != 0:
                    pull = (nObs-nExp)/nExpTotErDo
                if nObs == 0 and nExp == 0:
                    pull = 0
                    nObs = -100
                    nPred = -100
                    nExpEr = 0
                    nExpTotEr = 0
                    nExpStatEr = 0
                    nExpStatErUp = 0
                    nExpStatErDo = 0
                    nExpTotErUp = 0
                    nExpTotErDo = 0

                #bkg components
                compInfo = info[6]
                for i in range(len(compInfo)):
                    hbkgComponents[i].SetBinContent(counter+1,compInfo[i][1])

                break

        if nObs>max: max = nObs
        if nExp+nExpTotErUp > max: max = nExp+nExpTotErUp
        if nObs<min and nObs != 0: min = nObs
        if nExp<min and nExp != 0: min = nExp

        graph_bkg.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg.SetPointError(counter, 0.5, 0.5, nExpStatErDo, nExpStatErUp)
        graph_bkg2.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg2.SetPointError(counter, 0.5, 0.5, nExpEr, nExpEr)
        graph_bkg3.SetPoint(counter, hbkg.GetBinCenter(counter+1), nExp)
        graph_bkg3.SetPointError(counter, 0.5, 0.5, 0, 0)
        graph_data.SetPoint(counter, hbkg.GetBinCenter(counter+1), nObs)
        if not nObs > 0: nObs = 0
        graph_data.SetPointError(counter, 0., 0, sqrt(nObs), sqrt(nObs))

        graph_pull.SetPoint(counter, hbkg.GetBinCenter(counter+1), pull)
        graph_pull.SetPointError(counter, 0., 0, 0, 0)

        hdata.GetXaxis().SetBinLabel(counter+1, name)
        hdata.SetBinContent(counter+1, nObs)
        hdata.SetBinError(counter+1, sqrt(nObs))
        hbkg.SetBinContent(counter+1, nExp)
        hbkg.SetBinError(counter+1, nExpStatEr)

        hbkgUp.SetBinContent(counter+1, nExp+nExpTotErUp)
        hbkgDown.SetBinContent(counter+1, nExp-nExpTotErDo)

    hdata.SetMaximum(100*max)
    #if min<=0: min=0.5
    #hdata.SetMinimum(0.95*min)
    hdata.SetMinimum(0.)
    #hdata.SetMinimum(0.1)
    return

def MymakePullPlot(pickleFilename, regionList, samples, renamedRegions, outputPrefix, doBlind=True, outDir=outputDir,plotSignificance="",):
    """
    Make a pull plot from a pickle file of results

    @param pickleFilename Filename to open
    @param regionList List of regions to dxraw pulls for
    @param samples List of samples in each region
    @param renamedRegions List of renamed regions; dict of old => new names
    @param outputPrefix Prefix for the output file
    @param doBlind Blind the SR or not?
    @param plotSignificance: arxiv or atlas for recommendation to use to calculate significance
                             leave blank if no significance is calculated
    """
    try:
        picklefile = open(pickleFilename,'rb')
    except IOError:
        print "Cannot open pickle %s, continuing to next" % pickleFilename
        return

    mydict = pickle.load(picklefile)

    #print len(mydict["names"])
    #print len(mydict["nobs"])

    results1 = []
    results2 = []
    for region in mydict["names"]:
        # TODO: this is pretty bad. we should zip all these things.
        index = mydict["names"].index(region)
        try:
            nbObs = mydict["nobs"][index]
        except:
            nbObs = 0

        #if "CR" in region:
        #  nbExp = mydict["TOTAL_MC_EXP_BKG_events"][index]
        #  nbExpEr = mydict["TOTAL_MC_EXP_BKG_err"][index]
        #  #nbExpPostFit = mydict["TOTAL_FITTED_bkg_events"][index]
        #else:
        nbExp = mydict["TOTAL_FITTED_bkg_events"][index]
        nbExpEr = mydict["TOTAL_FITTED_bkg_events_err"][index]
        #  #nbExpPostFit = 0

        pEr = PoissonError(nbObs)
        totEr = sqrt(nbExpEr*nbExpEr+pEr[2]*pEr[2])
        totErDo = sqrt(nbExpEr*nbExpEr+pEr[1]*pEr[1])
        totErUp = sqrt(nbExpEr*nbExpEr+pEr[0]*pEr[0])

        pEr_pull = PoissonError(nbExp)
        totEr_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[2]*pEr_pull[2])
        totErDo_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[1]*pEr_pull[1])
        totErUp_pull = sqrt(nbExpEr*nbExpEr+pEr_pull[0]*pEr_pull[0])
 
        #if DATAMC:

            
        if plotSignificance == "arxiv":
            #calculates significance from https://arxiv.org/abs/1111.2062
            print "plot significance in the bottom panel"
            pValue = pValuePoissonError(int(nbObs), nbExp, nbExpEr*nbExpEr)
            print "pval:", pValue
            if pValue < 0.5:
                pull = pValueToSignificance(pValue, nbObs>nbExp )
                print pull
            else:
                pull = 0.0001
                print "pull at zero!"
        elif plotSignificance == "atlas":
            #significance calculated from https://cds.cern.ch/record/2643488
            # relabel variables to match CDS formula
            print 'calculating significance from W. Buttinger and M.Lefebvre recommendation'
            factor1 = nbObs*log( (nbObs*(nbExp+nbExpEr**2))/(nbExp**2+nbObs*nbExpEr**2) )
            factor2 = (nbExp**2/nbExpEr**2)*log( 1 + (nbExpEr**2*(nbObs-nbExp))/(nbExp*(nbExp+nbExpEr**2)) )

            if nbObs < nbExp:
                pull  = -sqrt(2*(factor1 - factor2))
            else:
                pull  = sqrt(2*(factor1 - factor2))
            print pull

        else:
            print "plot pull = (obs-exp)/err in the bottom panel"
            if (nbObs-nbExp) > 0 and totErUp_pull != 0:
                pull = (nbObs-nbExp)/totErUp_pull
            if (nbObs-nbExp) <= 0 and totErDo_pull != 0:
                pull = (nbObs-nbExp)/totErDo_pull
            print pull
            if -0.02 < pull < 0: pull = -0.02 ###ATT: ugly
            if 0 < pull < 0.02:  pull = 0.02 ###ATT: ugly

        nbExpComponents = []
        for sam in samples.split(","):
            #if "CR" in region:
            #    nbExpComponents.append((sam, mydict["MC_exp_events_"+sam][index] ))
            #else:
            nbExpComponents.append((sam, mydict["Fitted_events_"+sam][index] ))

        if "SR" in region and doBlind:
            nbObs = -100
            pull = 0

        if "CR" in region:
            if mydict["MC_exp_events_ttbar"][index] != 0:
                pull = mydict["Fitted_events_ttbar"][index]/mydict["MC_exp_events_ttbar"][index]
                print "ttbar SF: ", pull
            #elif mydict["MC_exp_events_ttbar"][index] == 0::
            #    pull = 0
            #    print "WARNING: ttbar SF: ", pull
        else:
            print "{0:s}: {1}".format(region, pull)

        print "region: {0} nObs {1}".format(region, nbObs)

        results1.append((region,pull,nbObs,nbExp,nbExpEr,totEr,nbExpComponents))

    #pull
    if plotSignificance =="":
        MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, 0.12)
    else:
        MyMakeHistPullPlot(samples, regionList, outputPrefix, results1, renamedRegions, doBlind, outDir, 0.12, None, True,True)
    # return the results array in case you want to use this somewhere else
    return results1


if __name__ == "__main__":
    main()


